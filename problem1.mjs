const problem1 = (dataSet) => {
  let emailAddresses = [];
  if (typeof dataSet == "object" && dataSet!== null && typeof dataSet.length=='number') {
    for (let individual of dataSet) {
      emailAddresses.push(individual.email);
    }
    return emailAddresses;
  } 
  else {
    console.log("invalid input");
    return;
  }
};

export default problem1;
