const problem2 = (dataSet, age) => {
  if (
        typeof dataSet == "object" &&
        dataSet !== null &&
        typeof dataSet.length == "number" &&
        typeof age == "number"
     ) {
        let hobbies = [];
        for (let individual of dataSet) {
            if (individual.age == age) {
                hobbies.push(individual.hobbies);
            }
          }
        return hobbies;
       } 
  else {
     return;
   }
};

export default problem2;
