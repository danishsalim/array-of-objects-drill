const problem3 =(dataSet)=>{
    if(typeof dataSet == "object" && dataSet!== null && typeof dataSet.length=='number'){
        for(let individual of dataSet){
            if(individual.isStudent && individual.country==='Australia'){
                console.log(individual.name);
            }
        }
    }
    else{
        return  null;
    }
}

export default problem3;