import dataSet from "../dataSet.mjs";
import problem2 from "../problem2.mjs";

const hobbies = problem2(dataSet, 30);
if (hobbies) {
  for (let hobby of hobbies) {
    console.log(hobby);
  }
}
